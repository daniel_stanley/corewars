package com.example.corewars;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.graphics.Color;
import android.util.Log;
//import android.widget.Toast;

public class Core {
	int size=8192;
	int processLimit=10;
	int[] instructions;
	int[] aModifier;
	int[] aField;
	int[] bModifier;
	int[] bField;
	public boolean parseSuccess;
	
	static boolean anInteger;//a really dumb way to know if the value being returned is an integer or an address
	
	static boolean twoPlayer;
	
	/**Note that the following block is never re-initialized (yet)**/
	int currentProcess=0;
	public int[] alphaProcesses=new int[processLimit];
	int alphaProcessNum=1;
	public int[] betaProcesses=new int[processLimit];
	int betaProcessNum=1;
	boolean turn=true;//true for alpha's turn, false for beta's
	int alphaCurrentProcess=0;//an index in alphaProcesses, always less than alphaProcessNum
	int betaCurrentProcess=0;
	
	public Core(int size){
		Log.v("tag1", "Creating");
		instructions=new int[size];
		aModifier=new int[size];
		aField=new int[size];
		bModifier=new int[size];
		bField=new int[size];
		
		//parseSuccess=init(input, 0);
		
		
	}


	public static int checkProgram(String program){
		String[] lines=program.split("\\n");
		for(int i=0;i<lines.length;i++){
			Pattern regex=Pattern.compile("(dat|mov|add|sub|jmp|jmz|jmn|cmp|slt|djn|spl) ([@#$])(-?[0-9]+) ([@#$])(-?[0-9]+)");

			Matcher matcher1=regex.matcher(lines[i]);
			if(!matcher1.find()){
				return i;
			}
		}
		return -1;//actually means it's good
	}
	public boolean init(String program, int offset){
		//betaProcesses[0]=30;//This is temporary, should eventually be randomized
		
		Log.v("tag1","Program:");
		Log.v("tag1", program);
		String[] lines=program.split("\\n");
		
		for(int i=0;i<lines.length;i++){
			Log.v("tag1", "i loop "+i);
			Log.v("tag1", "line length is  "+lines[i].length());
			Log.v("tag1", "Line:"+lines[i]);
			
			
			
			Pattern regex=Pattern.compile("(dat|mov|add|sub|jmp|jmz|jmn|cmp|slt|djn|spl) ([@#$])(-?[0-9]+) ([@#$])(-?[0-9]+)");
			
			Matcher matcher1=regex.matcher(lines[i]);
			if(matcher1.find()){
				String[] mnemonics={"dat", "mov", "add", "sub", "jmp", "jmz", "jmn", "cmp", "slt","djn","spl"};
				String sub=matcher1.group(1);
				
				for(int j=0;j<mnemonics.length;j++){
					Log.v("tag1", "j loop "+j);
					if(sub.equals(mnemonics[j])){
						instructions[i+offset]=j;
						Log.v("tag1", "Matched a mnemonic");
						break;
					}
				}//should always break out of this loop
				
				sub=matcher1.group(2);
				if(sub.equals("#")){
					aModifier[i+offset]=0;
				}else if(sub.equals("$")){
					aModifier[i+offset]=1;
				}else if(sub.equals("@")){
					aModifier[i+offset]=2;
				}else{
					//should never be reached
					return false;
				}
				
				aField[i+offset]=Integer.parseInt(matcher1.group(3));
				
				sub=matcher1.group(4);
				if(sub.equals("#")){
					bModifier[i+offset]=0;
				}else if(sub.equals("$")){
					bModifier[i+offset]=1;
				}else if(sub.equals("@")){
					bModifier[i+offset]=2;
				}else{
					//should never be reached
					return false;
				}
				
				bField[i+offset]=Integer.parseInt(matcher1.group(5));
				Log.v("tag1","Finished parsing line "+i);
				
				
			}else{
				Log.v("tag1","Pattern not Matched");
				return false;
			}
			
			
			/**
			String sub=lines[i].substring(0,3).toLowerCase();
			String[] mnemonics={"dat", "mov", "add", "sub", "jmp", "jmz", "jmn", "cmp", "slt","djn","spl"};
			boolean good=false;
			for(int j=0;j<mnemonics.length;j++){
				Log.v("tag1", "j loop "+j);
				if(sub.equals(mnemonics[j])){
					instructions[i+offset]=j;
					good=true;
					break;
				}
				
			}
			if(!good){
				Log.v("tag1", "Error parsing instruction mnemonic");
				return false;
			}
			
			Log.v("tag1", "tag A");
			
			sub=lines[i].substring(4,5).toLowerCase();
			if(sub.equals("#")){
				aModifier[i+offset]=0;
			}else if(sub.equals("$")){
				aModifier[i+offset]=1;
			}else if(sub.equals("@")){
				aModifier[i+offset]=2;
			}else{
				return false;
			}
			

			Log.v("tag1", "tag C");
			
			char subChar=lines[i].charAt(5);
			aField[i+offset]=(int)subChar-48;
			
			Log.v("tag1", "tag B");
			
			sub=lines[i].substring(7,8).toLowerCase();
			if(sub.equals("#")){
				bModifier[i+offset]=0;
			}else if(sub.equals("$")){
				bModifier[i+offset]=1;
			}else if(sub.equals("@")){
				bModifier[i+offset]=2;
			}else{
				return false;
			}
			
			subChar=lines[i].charAt(8);
			bField[i+offset]=(int)subChar-48;
			**/
			Log.v("tag1", "Ending loop "+i);
			
		}
		Log.v("tag1", "Finishing init");
		return true;
	}
	
	
	static int mod(int a, int b){
		return (a%b+b)%b;
	}
	
	boolean tick(int pos){
		int command=instructions[pos];
		switch(command){
		case 0:
			if(turn){
				Log.v("tag1","Process "+alphaCurrentProcess+" died");
				if(alphaProcessNum==1){
					//DEAD
					return false;
				}else{
					for(int i=alphaCurrentProcess;i<alphaProcessNum-1;i++){
						Log.v("tag1","i is "+i);
						Log.v("tag1",alphaProcesses[i]+" just became "+alphaProcesses[i+1]);
						alphaProcesses[i]=alphaProcesses[i+1];
					}
					alphaProcessNum--;
					alphaProcesses[alphaProcessNum]=0;//not necessary, but keeps the array clean
					alphaCurrentProcess=mod(alphaCurrentProcess-1,alphaProcessNum);//moves back to the last, will be moved forward again before next tick
					currentProcess=alphaProcesses[alphaCurrentProcess];
				}
			}else{
				if(betaProcessNum==1){
					//DEAD
					return false;
				}else{
					for(int i=betaCurrentProcess;i<betaProcessNum-1;i++){
						betaProcesses[i]=betaProcesses[i+1];
					}
					betaProcessNum--;
					betaProcesses[betaProcessNum]=0;//not necessary, but keeps the array clean
					betaCurrentProcess=mod(betaCurrentProcess-1,betaProcessNum);
				}
			}
			break;
			
			
		case 1:
			//copy
			int destination=0;
			//assumed at compile time that the b modifier is not immediate
			if(bModifier[pos]==1){
				destination=mod(bField[pos]+pos,size);
			}else{
				destination=mod(bField[mod(bField[pos]+pos,size)]+pos,size);
			}
			//destination=mod(destination+pos,size);
			
			Log.v("tag1","Copying from "+currentProcess+" to Destination: "+destination);
			
			
			if(aModifier[pos]==0){
				//source immediate
				bField[destination]=aField[pos];
				//MainActivity.say("Moving num "+aField[pos]+" to address "+destination);
			}else{
				//int destination=mod(bField[pos]+pos,size);//the +pos is because addresses are relative to this command;
				//Log.v("tag1", "Destination "+destination);
				int source=0;
				if(aModifier[pos]==1){
					source=mod(aField[pos]+pos,size);
				}else{
					source=mod(bField[mod(aField[pos]+pos,size)]+pos,size);
				}
				instructions[destination]=instructions[source];
				aModifier[destination]=aModifier[source];
				aField[destination]=aField[source];
				bModifier[destination]=bModifier[source];
				bField[destination]=bField[source];
				//MainActivity.say("Moving address "+source+" to address "+destination);
			}
			currentProcess+=1;
			return true;
			
		case 2:
			//add
			int destination2=0;
			//assumed at compile time that the b modifier is not immediate
			if(bModifier[pos]==1){
				destination2=mod(bField[pos]+pos,size);//direct
			}else{
				destination2=mod(bField[mod(bField[pos]+pos,size)]+pos,size);//indirect
			}
			//destination2=mod(destination2+pos,size);
			
			//int first=0;
			if(aModifier[pos]==0){
				//immediate, add first to destination b field
				bField[destination2]+=aField[pos];
				Log.v("tag1","destination2: "+destination2);
				Log.v("tag1","adding: "+aField[pos]);
				//MainActivity.say("Adding "+aField[pos]+" to address "+destination2);
			}else{
				int source2=0;
				if(aModifier[pos]==1){
					source2=mod(pos+bField[pos],size);
				}else{//indirect
					source2=mod(bField[mod(bField[pos]+pos,size)]+pos,size);//indirect
				}
				aField[destination2]+=aField[source2];
				bField[destination2]+=bField[source2];
			}
			currentProcess+=1;
			return true;
		case 3:
			//subtract
			int destination3=0;
			//assumed at compile time that the b modifier is not immediate
			if(bModifier[pos]==1){
				destination3=mod(bField[pos]+pos,size);//direct
			}else{
				destination3=mod(bField[mod(bField[pos]+pos,size)]+pos,size);//indirect
			}
			//destination3=mod(destination3+pos,size);
			
			//int first=0;
			if(aModifier[pos]==0){
				//immediate, add first to destination b field
				bField[destination3]-=aField[pos];
			}else{
				int source2=0;
				if(aModifier[pos]==1){
					source2=mod(pos+bField[pos],size);
				}else{//indirect
					source2=mod(bField[mod(bField[pos]+pos,size)]+pos,size);//indirect
				}
				aField[destination3]-=aField[source2];
				bField[destination3]-=bField[source2];
			}
			currentProcess+=1;
			return true;
		case 4:
			//goto, only look at A field, assumed not immediate
			int destination4=0;
			if(aModifier[pos]==1){
				destination4=mod(aField[pos]+pos,size);//direct
			}else{
				destination4=mod(bField[mod(aField[pos]+pos,size)]+pos,size);//indirect
			}
			Log.v("tag1","Jumping to "+destination4);
			currentProcess=destination4;
			return true;
		case 5:
			//jmz
			if(getIntB(pos)==0){
				int destination5=0;
				if(aModifier[pos]==1){
					destination5=mod(aField[pos]+pos,size);//direct
				}else{
					destination5=mod(bField[mod(aField[pos]+pos,size)]+pos,size);//indirect
				}
				currentProcess=destination5;
			}else{
				currentProcess++;
			}
			return true;
		case 6:
			//jmn
			if(getIntB(pos)!=0){
				int destination5=0;
				if(aModifier[pos]==1){
					destination5=mod(aField[pos]+pos,size);//direct
				}else{
					destination5=mod(bField[mod(aField[pos]+pos,size)]+pos,size);//indirect
				}
				currentProcess=destination5;
			}else{
				currentProcess++;
			}
			return true;
		case 7:
			//cmp
			if(getIntA(pos)==getIntB(pos)){
				currentProcess+=2;
			}else{
				currentProcess++;
			}
			return true;
		case 8:
			//cmp
			if(getIntB(pos)<getIntA(pos)){
				currentProcess+=2;
			}else{
				currentProcess++;
			}
			return true;
		case 10:
			//spl
			int newPosition;
			if(aModifier[pos]==1){
				newPosition=mod(aField[pos]+pos,size);//direct
			}else{
				newPosition=mod(bField[mod(aField[pos]+pos,size)]+pos,size);//indirect
			}
			if(turn){
				if(alphaProcessNum<processLimit){
					alphaProcessNum++;
					for(int i=alphaProcessNum-1;i>alphaCurrentProcess;i--){
						alphaProcesses[i+1]=alphaProcesses[i];//shift them down to make room for the new one
					}

					alphaProcesses[alphaCurrentProcess+1]=newPosition;
					currentProcess++;
				}else{
					MainActivity.say("spl failed, too many processes");
				}
			}else{
				if(betaProcessNum<processLimit){
					betaProcessNum++;
					for(int i=betaProcessNum-1;i>betaCurrentProcess;i--){
						betaProcesses[i+1]=betaProcesses[i];//shift them down to make room for the new one
					}

					betaProcesses[betaCurrentProcess+1]=newPosition;
					currentProcess++;
				}else{
					MainActivity.say("spl failed, too many processes");
				}
			}
		}
		return true;//should never be reached, should always choose a case
	}
	
	/**
	int getAddress(int pos){
		if()
	}
	**/
	
	//get the value/command specified by the aField of the command at the address pos
	//whether it's an address or an integer is designated by anInteger
	int getValue(int pos){
		pos=mod(pos,size);
		int value=0;
		if(aModifier[pos]==0){
			value=aField[pos];
			anInteger=true;
		}else if(aModifier[pos]==1){
			value=aField[pos];
			anInteger=false;
		}else if(aModifier[pos]==2){
			value=getValue(aField[pos]);
		}else{
			//an  occurred
			//0 will be returned
		}
		return value;
	}
	
	
	int getIntA(int pos){
		int num=0;
		if(aModifier[pos]==0){
			num=aField[pos];//immediate
		}else if(aModifier[pos]==1){
			num=aField[mod(aField[pos]+pos,size)];//direct
		}else{
			num=aField[mod(aField[mod(aField[pos]+pos,size)]+pos,size)];//indirect
		}
		return num;
	}
	
	int getIntB(int pos){
		int num=0;
		if(bModifier[pos]==0){
			num=bField[pos];//immediate
		}else if(bModifier[pos]==1){
			num=bField[mod(bField[pos]+pos,size)];//direct
		}else{
			num=bField[mod(bField[mod(bField[pos]+pos,size)]+pos,size)];//indirect
		}
		return num;
	}
	
	void run(int iterations){
		for(int i=0;i<iterations;i++){
			if(!tick2()){
				Log.v("tag1","DEAD on iteration "+i);
				break;
			}
			
		}
	}
	
	public boolean tick2(){
		//Log.v("tag1", "Tag D");
		boolean temp=tick(currentProcess);
		
		if(turn){
			alphaProcesses[alphaCurrentProcess]=currentProcess;
			alphaCurrentProcess=(alphaCurrentProcess+1)%alphaProcessNum;
		}else{
			betaProcesses[betaCurrentProcess]=currentProcess;
			betaCurrentProcess=(betaCurrentProcess+1)%betaProcessNum;
		}
		if(twoPlayer){
			turn=!turn;
		}
		
		if(turn){
			currentProcess=alphaProcesses[alphaCurrentProcess];
		}else{
			currentProcess=betaProcesses[betaCurrentProcess];
		}
		return temp;
		
	}
	
	

	
}
