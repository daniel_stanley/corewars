package com.example.corewars;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.AttributeSet;
//import android.graphics.RectF;
import android.util.Log;
import android.view.View;

public class MyView extends View{
	
	int rectHeight;
	int rectWidth;
	int across;
	int down;
	public static int targetSize=500;
	double spaceAcross;
	double spaceDown;
	
	
	
	Rect rect1=new Rect(0,0,100,100);
	Rect rect2=new Rect(0,0,200,200);
	Paint paint1=new Paint();
	public static Core core2=null;
	
	boolean survived=true;
	boolean first=true;
	Long nextUpdate=0L;
	
	

	public MyView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		//init();
	}

	public MyView(Context context, AttributeSet attrs) {
		super(context);
		// TODO Auto-generated constructor stub
		//init();
	}
	
	
	@Override
	public void onDraw(Canvas canvas){
		Long currentTime=SystemClock.uptimeMillis();

		if(currentTime>nextUpdate){
			survived=MyView.core2.tick2();
			init(canvas);
			Log.v("tag1", "Boolean survived is "+survived);
			nextUpdate=currentTime+100;
		}


		//Log.v("tag1","Height: "+canvas.getHeight()+"  Width: "+canvas.getWidth());
		for(int i=0;i<across*down&&i<targetSize;i++){
			int x=i%across;
			int y=i/across;
			rect1.offsetTo((int)(spaceAcross*x), (int)(spaceDown*y));

			paint1.setColor(getColor(core2.instructions[i]/10.0,((-(i+1)*(1.0/(across*down)))%1.0+1.0)%1.0));
			canvas.drawRect(rect1, paint1);
		}
		int position=core2.currentProcess;

		if(position<across*down&&position<targetSize){
			int x=position%across;
			int y=position/across;
			if(core2.turn){
				paint1.setColor(Color.BLACK);
			}else{
				paint1.setColor(Color.WHITE);
			}
			rect2.offsetTo((int)(spaceAcross*x-spaceAcross*.25), (int)(spaceDown*y-spaceDown*.25));
			canvas.drawRect(rect2, paint1);

			rect1.offsetTo((int)(spaceAcross*x), (int)(spaceDown*y));

			paint1.setColor(getHue(core2.instructions[position]/10.0));

			//paint1.setColor(Color.GREEN);
			canvas.drawRect(rect1, paint1);
		}







		if(survived){
			invalidate();
		}




	}

	
	
	
	
	public static int getColor(double hue, double saturation){
		int Case=(int)(6.0*hue);
        double ratio=(6.0*hue)%1;
        double r=0;
        double g=0;
        double b=0;
        switch(Case){
            case 0:
                r=255;
                g=255*ratio;
                b=0;
                break;
            case 1:
                r=255-255*ratio;
                g=255;
                b=0;
                break;
            case 2:
                r=0;
                g=255;
                b=255*ratio;
                break;
            case 3:
                r=0;
                g=255-255*ratio;
                b=255;
                break;
            case 4:
                r=255*ratio;
                g=0;
                b=255;
                break;
            case 5:
                r=255;
                g=0;
                b=255-255*ratio;
                break;
        }
        r*=saturation;
        g*=saturation;
        b*=saturation;
        return  Color.rgb((int) r,(int) g,(int) b);
	}
	
	
	public static int getHue(double hue){//hue should be between 0 and 1
        int Case=(int)(6.0*hue);
        double ratio=(6.0*hue)%1;
        double r=0;
        double g=0;
        double b=0;
        switch(Case){
            case 0:
                r=255;
                g=255*ratio;
                b=0;
                break;
            case 1:
                r=255-255*ratio;
                g=255;
                b=0;
                break;
            case 2:
                r=0;
                g=255;
                b=255*ratio;
                break;
            case 3:
                r=0;
                g=255-255*ratio;
                b=255;
                break;
            case 4:
                r=255*ratio;
                g=0;
                b=255;
                break;
            case 5:
                r=255;
                g=0;
                b=255-255*ratio;
                break;
        }
        
        return  Color.rgb((int) r,(int) g,(int) b);
    }

	void init(Canvas canvas){
		Log.v("tag1","being initialized");
		int width=canvas.getWidth();
		int height=canvas.getHeight();
		Log.v("tag1",width+" width "+height+" height");
		double ratio=(double)height/width;
		across=(int) (Math.sqrt(targetSize/ratio)+.5);
		down=(int) (across*ratio+.5);
		spaceAcross=(double)width/across;
		spaceDown=(double)height/down;
		rectWidth=(int)spaceAcross+1;
		rectHeight=(int)spaceDown+1;
		rect1=new Rect(0,0,rectWidth,rectHeight);
		rect2=new Rect(0,0,(int)(rectWidth*1.5),(int)(rectHeight*1.5));
		Log.v("tag1",across+" across "+down+" down");
	}
	
	
}
