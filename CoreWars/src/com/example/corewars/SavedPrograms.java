package com.example.corewars;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public abstract class SavedPrograms {
	static SharedPreferences sharedPref;
	
	/**
	public SavedPrograms(Context context){
		//sharedPref=context.getSharedPreferences("com.example.corewars.savedData", Context.MODE_PRIVATE);
	}
	**/
	public static void init(Context context){
		sharedPref=context.getSharedPreferences("com.example.corewars.savedData", Context.MODE_PRIVATE);
	}
	
	public static String[] getNames(){
		String names=sharedPref.getString("names","");
		String[] temp=names.split("\\n");
		if(temp==null){
			String[] temp2={};
			temp=temp2;
		}
		return temp;
	}
	
	public static boolean updateProgram(String name, String program){
		if(!sharedPref.contains(name)){
			return false;
		}else{
			SharedPreferences.Editor editor=sharedPref.edit();
			editor.putString(name, program);
			editor.commit();
			return true;
		}
	}
	
	public static boolean newProgram(String name, String program){
		if(sharedPref.contains(name)){
			return false;
		}else{
			SharedPreferences.Editor editor=sharedPref.edit();
			editor.putString(name, program);
			String names=sharedPref.getString("names", "");
			names=name+"\n"+names;
			editor.putString("names", names);
			editor.commit();
			return true;
		}
	}

	public static String getProgram(String name){
		return sharedPref.getString(name,  "");
	}

	//returns true if the spinner has to be updated
	public static boolean save(String name, String program){
		if(name.equals("New")){
			MainActivity.say("Change the name first");
			return false;
		}else{
			Log.v("tag1","Still trying to save");
			if(newProgram(name, program)){
				return true;
			}else{
				updateProgram(name,program);
				return false;
			}
		}
	} 
	
	
	
}
