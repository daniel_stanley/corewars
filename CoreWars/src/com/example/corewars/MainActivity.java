package com.example.corewars;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
//import android.graphics.Canvas;
//import android.graphics.Paint;
//import android.graphics.Rect;
import android.util.Log;
import android.view.Menu;
//import android.view.SurfaceHolder;
import android.view.View;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemSelectedListener{
	static Context context;
	Core core1=null;
	static String program1="";
	static String program2="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//((TextView)findViewById(R.id.editText1)).setText(program1);
		core1=new Core(8192);
		
		SavedPrograms.init(this);
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
		spinner.setOnItemSelectedListener(this);
		updateSpinner();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		context=this;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	public void testCompile(View v){
		int parseSuccess=Core.checkProgram(((TextView)(findViewById(R.id.program))).getText().toString());
		if(parseSuccess==-1){
			Toast.makeText(this,"Program Compiled Successfully",Toast.LENGTH_SHORT).show();
		}else{
			Toast.makeText(this,"ERROR ON LINE "+(parseSuccess+1),Toast.LENGTH_LONG).show();
		}
	}
	
	public void clearCore(View v){
		core1=new Core(8192);
	}
	
	/**
	public void changeView(View v){
		if(program2.equals("")){
			Core.twoPlayer=false;
		}else{
			Core.twoPlayer=true;
		}
		Log.v("tag1", "About to set Content view");
		SpecialView specialView1=new SpecialView(this);
		setContentView(specialView1);
		SurfaceHolder holder= specialView1.getHolder();
		Canvas canvas=holder.lockCanvas();
		
		Log.v("tag1", "Tag A");
		
		Rect rect1=new Rect(0,10,0,20);
		Paint paint1=new Paint();
		paint1.setColor(888888);
		Log.v("tag1", "Tag C");
		//canvas.drawRect(rect1, paint1);
		
		Log.v("tag1", "Tag B");
		
		//holder.unlockCanvasAndPost(canvas);
		Log.v("tag1", "finishing changeView");
	}
	**/
	
	public void changeView2(View v){
		Log.v("tag1", "About to set Content view");
		if(program2.equals("")){
			Core.twoPlayer=false;
		}else{
			Core.twoPlayer=true;
		}
		

		
		Log.v("tag1", "Tag A");
		if(!core1.equals(null)){
			MyView.core2=core1;
			
			MyView myView1=new MyView(this);
			setContentView(myView1);


		}
	}

	public void changeView3(View v){
		Log.v("tag1", "About to set Content view");
		if(program2.equals("")){
			Core.twoPlayer=false;
		}else{
			Core.twoPlayer=true;
		}
		

		
		Log.v("tag1", "Tag A");
		if(!core1.equals(null)){
			MyView.core2=core1;
			
			//MyView myView1=new MyView(this);
			//setContentView(myView1);
			
			Intent intent=new Intent(this, WarRoom.class);
			//String extraKey="com.example.corewars.THECORE";
			//intent.putExtra(extraKey,core1);
			startActivity(intent);

		}
	}
	
	public void changeView4(View v){
		Log.v("tag1", "About to enter waiting room");
		Intent intent=new Intent(this, WaitingRoom.class);
		startActivity(intent);
	}
	
	/**
	public void load(View v){
		((TextView)findViewById(R.id.editText1)).setText(program1);
		((TextView)findViewById(R.id.editText2)).setText(program2);
	}
	**/

	public static void say(String words){
		Toast.makeText(context,words,Toast.LENGTH_SHORT).show();
	}

	void updateSpinner(){

		ArrayAdapter<CharSequence> list=new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item);
		String[] items=SavedPrograms.getNames();
		for(String name:items){
			if(name!=""){
				list.add((CharSequence)name);
			}
		}
		list.add("New");
		list.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner spinner=(Spinner)findViewById(R.id.spinner1);
		spinner.setAdapter(list);
	}
	
	public void saveCurrent(View v){
		Log.v("tag1","Trying to save");
		if(SavedPrograms.save(  ((TextView)findViewById(R.id.name)).getText().toString()  ,((TextView)findViewById(R.id.program)).getText().toString())){
			updateSpinner();
		}
	}


	/**
	public static void loadProgram(String name) {
		context.loadProgram2();
		
	}
	
	void loadProgram2(String name){
		String program=SavedPrograms.getProgram(name);
		((TextView)findViewById(R.id.name)).getText();
	}
	**/
	
	public void onItemSelected(AdapterView<?> parent, View view, 
            int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
		
		String name=parent.getItemAtPosition(pos).toString();
		String program=SavedPrograms.getProgram(name);
		((TextView)findViewById(R.id.name)).setText(name);
		((TextView)findViewById(R.id.program)).setText(program);
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
}
