package com.example.corewars;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class WaitingRoom extends Activity implements OnItemSelectedListener{
	String warriorA;
	String warriorB;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_room);
        Spinner spinnerA = (Spinner) findViewById(R.id.spinner1);
		spinnerA.setOnItemSelectedListener(this);
		Spinner spinnerB = (Spinner) findViewById(R.id.spinner2);
		spinnerB.setOnItemSelectedListener(this);
		
		
		ArrayAdapter<CharSequence> list=new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item);
		String[] items=SavedPrograms.getNames();
		for(String name:items){
			if(name!=""){
				list.add((CharSequence)name);
			}
		}
		list.add("New");
		list.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerA.setAdapter(list);
		spinnerB.setAdapter(list);
	}
	
	
	
	public void onItemSelected(AdapterView<?> parent, View view, 
            int pos, long id) {
        
		if(parent.getId()== R.id.spinner1){
			warriorA=SavedPrograms.getProgram(parent.getItemAtPosition(pos).toString());
			Log.v("tag1","Warrior a init");
		}else{
			warriorB=SavedPrograms.getProgram(parent.getItemAtPosition(pos).toString());
			Log.v("tag1","Warrior b init");
		}
		
		Log.v("tag1","Stuff:\t"+pos+"\tand "+parent.getId());
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
    
    public void begin(View v){
    	int size=500;
    	MyView.core2=new Core(500);
    	int pos=(int)(size*Math.random());
    	MyView.core2.init(warriorA, pos);
    	MyView.core2.alphaProcesses[0]=pos;
    	MyView.core2.currentProcess=pos;
    	pos=(int)(size*Math.random());
    	MyView.core2.betaProcesses[0]=pos;
    	MyView.core2.init(warriorB, pos);
    	Core.twoPlayer=true;
    	MyView.targetSize=size;
    	Intent intent=new Intent(this, WarRoom.class);
		//String extraKey="com.example.corewars.THECORE";
		//intent.putExtra(extraKey,core1);
		startActivity(intent);
    	
    }
       
}
