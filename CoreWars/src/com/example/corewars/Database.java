package com.example.corewars;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper{
	
	private final String DATABASE_CREATE="create table programs(_id integer primary key autoincrement, name text not null, program text not null";

	public Database(Context context) {
		super(context, "programs.db", null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	public Cursor getNames(){
		return getReadableDatabase().rawQuery("select * from programs", null);
	}
	
	public boolean addProgram(String name, String program){
		SQLiteDatabase tempdb=getReadableDatabase();
		Cursor cursor=tempdb.rawQuery("select * from programs where name = "+name, null);
		tempdb.close();
		if(cursor!=null){
			return false;//name taken
		}
		
		ContentValues values=new ContentValues();
		values.put("name",name);
		values.put("program", program);
		SQLiteDatabase db=getWritableDatabase();
		db.insert("program",null,values);//returns long rowID
		db.close();
		return true;
	}
	
	public void deleteName(String name){
		SQLiteDatabase db=getWritableDatabase();
		db.delete("programs","name = "+name,null);
		db.close();
	}

	public void update(String name, String program){
		ContentValues values=new ContentValues();
		values.put("name",name);
		values.put("program", program);
		SQLiteDatabase db=getWritableDatabase();
		db.update("programs",values, "name = "+name,null);
		db.close();
	}
	
	public String getProgram(String name){
		SQLiteDatabase db=getReadableDatabase();
		Cursor cursor =db.rawQuery("select program from programs where name = "+name,null);
		cursor.moveToFirst();//assumed not null
		return cursor.getString(0);
	}
	
	

}
